# Fake News Backend

## Getting started

### Using `Python` and `pip`
Create python virtual environment
```shell
### on Windows ###
python -m venv venv
### on Linux/macOS ###
python3 -m venv venv
```
Activate the virtual environment
```shell
### on Windows ###
.\venv\Scripts\activate.bat
### on Linux/macOS ###
source ./venv/bin/activate
```

Install all dependencies in requirements.txt
```shell
pip install -r requirements.txt
```

Start the server
```shell
python -m app
```

### Using `docker` and `docker-compose`

```shell
docker-compose up --build -d
```
