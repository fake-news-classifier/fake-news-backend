FROM python:3.8.5

# Download language detection model
RUN wget "https://dl.fbaipublicfiles.com/fasttext/supervised-models/lid.176.bin"

# Copy requirements.txt and install all dependencies
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

# Copy all files in server directory
COPY . .

# Expose server port
EXPOSE 7000

# Run FastAPI application
CMD ["python", "-m", "app"]
