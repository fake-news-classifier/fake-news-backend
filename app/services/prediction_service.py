import re

import fasttext
import joblib
import nltk
from goose3 import Goose
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer

from app.exeptions.UnavailableLanguageException import UnavailableLanguageException
from app.exeptions.TooShortTextException import TooShortTextException

nltk.download('punkt')
nltk.download('stopwords')

available_language = ['__label__it', '__label__en']
fmodel = fasttext.load_model("lid.176.bin")

pipeline_en = joblib.load(open('model.joblib', 'rb'))
pipeline_it = joblib.load(open('model_it.joblib', 'rb'))

stopwords_en = set(stopwords.words("english"))
stopwords_it = set(stopwords.words("italian"))

stemmer_en = SnowballStemmer('english')
stemmer_it = SnowballStemmer('italian')


def predict_language(text: str):
    result = fmodel.predict(str(text))
    return result[0][0]


def get_stopwords(language: str):
    if language == "__label__it":
        return stopwords_it
    return stopwords_en


def get_stemmer(language: str):
    if language == "__label__it":
        return stemmer_it
    return stemmer_en


def get_model(language: str):
    if language == "__label__it":
        return pipeline_it
    return pipeline_en


def clean_text(text: str):
    # all lowercase
    text = text.lower()

    # remove escape sequences
    text = re.sub(r'(\r\n|\n|\r|\t)', ' ', text)

    # remove urls
    text = re.sub(r'https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)', ' ', text)

    # remove html
    text = re.sub(r'<.*?>', ' ', text)

    # remove emoj
    text = re.sub(r"["
                  u"\U0001F600-\U0001F64F"  # emoticons
                  u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                  u"\U0001F680-\U0001F6FF"  # transport & map symbols
                  u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                  u"\U00002702-\U000027B0"
                  u"\U000024C2-\U0001F251"
                  "]+", " ", text, flags=re.UNICODE)

    # remove all not alphanumeric character
    text = re.sub("[^a-z]+", " ", text)
    return text


def remove_stop_words(text: str, language: str):
    stop_words = get_stopwords(language)
    res = [word for word in text.split() if word not in stop_words]
    return " ".join(res)


def stemming(text: str, language: str):
    stemmer = get_stemmer(language)
    res = [stemmer.stem(word) for word in text.split()]
    return " ".join(res)


def process(site_html: str):
    text = Goose().extract(raw_html=site_html).cleaned_text

    if len(text) < 400 and len(text.splitlines()) < 10 and text.count('.') < 10:
        raise TooShortTextException

    # Text cleaning
    text = clean_text(text)

    # Language Prediction
    language = predict_language(text)
    if language not in available_language:
        raise UnavailableLanguageException

    # Text Preprocessing
    text = remove_stop_words(text, language)
    text = stemming(text, language)

    # Fake or Real Prediction
    prediction = get_model(language).predict([text])[0]

    return language, prediction
