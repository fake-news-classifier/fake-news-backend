from fastapi import APIRouter

from . import healthcheck_router
from . import prediction_router

router = APIRouter()

router.include_router(healthcheck_router.router, prefix='/healthcheck', tags=['health-check'])
router.include_router(prediction_router.router, prefix='/predict', tags=['predict'])
