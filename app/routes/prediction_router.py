from fastapi import APIRouter, HTTPException
from pydantic import BaseModel

from ..exeptions.TooShortTextException import TooShortTextException
from ..exeptions.UnavailableLanguageException import UnavailableLanguageException
from ..services import prediction_service

router = APIRouter()


class ParagraphsModel(BaseModel):
    siteUrl: str
    siteHtml: str


class PredictionResponseModel(BaseModel):
    prediction: str
    language: str


@router.post("/", response_model=PredictionResponseModel)
async def predict(request: ParagraphsModel):
    try:
        print(request.siteUrl)

        language, prediction = prediction_service.process(request.siteHtml)
        print(language, prediction)
        return PredictionResponseModel(
            prediction=prediction,
            language=language
        )
    except TooShortTextException as tooShortTextException:
        print(tooShortTextException)
        raise HTTPException(status_code=422, detail="Too Short Text Exception")
    except UnavailableLanguageException as unavailableLanguageException:
        print(unavailableLanguageException)
        raise HTTPException(status_code=422, detail="Unavailable Language Exception")
    except Exception as exception:
        print(exception)
        # LOGGER.error('An exception occurred: {}'.format(exception))
        raise HTTPException(status_code=500, detail="Prediction Exception")
